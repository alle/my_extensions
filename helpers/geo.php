<?php defined('SYSPATH') OR die('No direct access allowed.');

class geo_Core {

	public static function get_lat_long($address)
	{
		if ( ! is_string($address))
			die('All Addresses must be passed as a string');

		$url = sprintf('http://maps.google.com/maps?output=js&q=%s', rawurlencode($address));

		$result = FALSE;
		$coords = array(
			'lat' => 0,
			'lng' => 0,
		);

		if ($result = @file_get_contents($url))
		{
			if (strpos($result, 'errortips') > 1
				OR strpos($result,'Did you mean:') !== FALSE)
				return FALSE;

			preg_match('!center:\s*{lat:\s*(-?\d+\.\d+),lng:\s*(-?\d+\.\d+)}!U', $result, $matches);

			$coords['lat'] = $matches[1];
			$coords['lng'] = $matches[2];
		}

		return $coords;
	}

	public static function compose_indirizzo($dug, $toponimo, $num_civico, $comune, $provincia, $regione)
	{
		$indirizzo = trim(rtrim("$dug $toponimo $num_civico"), ', ').', ';
		
		$indirizzo .= "$comune, ";
		
		if ($comune != $provincia)
		{
			$indirizzo .= "$provincia, ";
		}

		$indirizzo .= "$regione";

		$indirizzo = rtrim($indirizzo, ', ').', Italy';

		return $indirizzo;
	}

} // End geo_Core