<?php defined('SYSPATH') OR die('No direct access allowed.');

class config_Core {

	public static function get($key)
	{
		$pieces = explode('.', $key);
		$file = array_shift($pieces);
		$pieces = implode('.', $pieces);
		$default_path = "default_$file.".$pieces;
		$default = Kohana::config("default_$file", FALSE, FALSE);
		isset($default) OR $default = NULL;
		$standard = Kohana::config($file, FALSE, FALSE);
		isset($standard) OR $standard = NULL;
		$merge = arr::merge($default, $standard);
		Kohana::config_set($file, $merge);
		return Kohana::config($key);
	}

} // End config