<?php defined('SYSPATH') OR die('No direct access allowed.');

class feed extends feed_Core {

	/**
	 * Creates a feed from the given parameters.
	 *
	 * @param   array   feed information
	 * @param   array   items to add to the feed
	 * @param   string  define which format to use
	 * @param   string  define which encoding to use
	 * @return  string
	 */
	public static function create($info, $items, $format = 'rss2', $encoding = 'UTF-8')
	{
		$info += array('title' => 'Generated Feed', 'link' => '', 'generator' => 'KohanaPHP');


		$feed = '<?xml version="1.0" encoding="'.$encoding.'"?><rss version="2.0"><channel></channel></rss>';
		// $feed = simplexml_load_string($feed);

		$feed = new SimpleXMLExtended($feed);

		foreach ($info as $name => $value)
		{
			if (($name === 'pubDate' OR $name === 'lastBuildDate') AND (is_int($value) OR ctype_digit($value)))
			{
				// Convert timestamps to RFC 822 formatted dates
				$value = date(DATE_RFC822, $value);
			}
			elseif (($name === 'link' OR $name === 'docs') AND strpos($value, '://') === FALSE)
			{
				// Convert URIs to URLs
				$value = url::site($value, 'http');
			}
			elseif ($name === 'image' AND is_array($value))
			{
				$value = arr::merge(array('url' => '', 'title' => '', 'link' => ''), $value);

				$image = $feed->channel->addChild($name);

				foreach ($value as $_key => $_value)
				{
					$image->addCData($_key, $_value);
				}

				continue;
			}

			// Add the info to the channel
			$feed->channel->addChild($name, $value);
		}

		foreach ($items as $item)
		{
			// Add the item to the channel
			$row = $feed->channel->addChild('item');

			foreach ($item as $name => $value)
			{
				if ($name === 'pubDate' AND (is_int($value) OR ctype_digit($value)))
				{
					// Convert timestamps to RFC 822 formatted dates
					$value = date(DATE_RFC822, $value);
				}
				elseif (($name === 'link' OR $name === 'guid') AND strpos($value, '://') === FALSE)
				{
					// Convert URIs to URLs
					$value = url::site($value, 'http');
				}
				elseif ($name === 'description' OR $name === 'title')
				{
					$row->addCData($name, $value);

					continue;
				}
				elseif ($name === 'enclosure' AND is_array($value))
				{
					$value = arr::merge(array('url' => '', 'length' => '', 'type' => ''), $value);

					$image = $row->addChild($name);

					foreach ($value as $_key => $_value)
					{
						$image->addAttribute($_key, $_value);
					}

					continue;
				}

				// Add the info to the row
				$row->addChild($name, $value);
			}
		}

		return $feed->asXML();
	}

} // End feed