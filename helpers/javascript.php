<?php defined('SYSPATH') OR die('No direct access allowed.');

class javascript_Core {

	static protected $javascripts = array();
	static protected $source;

	public static function add($files = array(), $unique = FALSE)
	{
		if (is_string($files))
		{
			$files = array($files);
		}

		foreach ($files as $key => $file)
		{
			self::$javascripts[$file] = $unique;
		}
	}

	public static function remove($files = array())
	{
		if (is_string($files))
		{
			$files = array($files);
		}

		foreach (self::$javascripts as $key => $file)
		{
			if (in_array($file, self::$javascripts))
			{
				unset(self::$javascripts[$key]);
			}
		}
	}

	static public function render($print = FALSE, $unique_name = 'time')
	{
		$output = '';
		foreach (self::$javascripts as $script => $unique)
		{
			// $output .= html::script($script, FALSE, $unique, $unique_name);
			$output .= javascript::script($script, FALSE, $unique, $unique_name);
		}

		if ($print)
		{
			echo $output;
		}

		return $output;
	}

	public static function add_source($source = '')
	{
		self::$source .= $source."\n";
	}

	public static function render_source($print = FALSE)
	{
		if ($print)
		{
			echo self::$source;
		}

		return self::$source;
	}

	public static function script($script, $index = FALSE, $unique = FALSE, $unique_name = 'time')
	{
		$compiled = '';

		if (is_array($script))
		{
			foreach ($script as $name)
			{
				$compiled .= javascript::script($name, $index, $unique, $unique_name);
			}
		}
		else
		{
			if (strpos($script, '://') === FALSE)
			{
				// Add the suffix only when it's not already present
				$script = url::base((bool) $index).$script;
			}

			if (substr_compare($script, '.js', -3, 3, FALSE) !== 0)
			{
				// Add the javascript suffix
				$script .= '.js';
			}

			if ($unique)
			{
				$script .= '?'.$unique_name.'='.$unique;
			}

			$compiled = '<script type="text/javascript" src="'.$script.'"></script>';
		}

		return $compiled."\n";
	}

} // End javascript_Core