<?php defined('SYSPATH') or die('No direct access allowed.');

class dt {

	public static function data( & $input, & $columns, $total_count, $filtered_count, & $data, $as_object = FALSE)
	{
		$output = ($as_object === FALSE)
			? self::__as_array($data, $columns)
			: self::__as_object($data, $columns);

		return array(
			'sEcho'                => (int) arr::get($input, 'sEcho'),
			'iTotalRecords'        => $total_count,
			'iTotalDisplayRecords' => $filtered_count, // $filtered ? count($data) : $total,
			'aaData'               => $output,
		);
	}

	private static function __clear_col_name($col)
	{
		$split = explode('.', $col);

		$col = array_pop($split);

		return $col;
	}

	private static function __as_array($data, $columns)
	{
		$output = array();

		foreach ($data as $row)
		{
			$_row = array();

			foreach ($columns as $col)
			{
				$col = self::__clear_col_name($col);

				$_row[] = $row->{$col};
			}

			$output[] = $_row;
		}

		return $output;
	}

	private static function __as_object($data, $columns)
	{
		$output = array();

		foreach ($data as $k => $row)
		{
			$_row = new stdClass;

			foreach ($columns as $col)
			{
				$col = self::__clear_col_name($col);

				$_row->{$col} = $row->{$col};
			}

			$output[] = $_row;
		}

		return $output;
	}

	public static function table_head($entity)
	{
		$config = Kohana::config('datatable.'.$entity);

		$output = '';

		foreach ($config as $field => $data)
		{
			if (arr::get($data, 'show') === TRUE)
			{
				$output .= '<th';
				if ($class = arr::get($data, 'class'))
				{
					$output .= ' class="'.$class.'"';
				}
				if ($title = arr::get($data, 'title'))
				{
					$output .= ' title="'.$title.'"';
				}
				$output .= '>'.$data['heading'].'</th>'.PHP_EOL;
			}
		}

		return $output;
	}

	public static function mdataprop($entity, $render = FALSE)
	{
		$config = Kohana::config('datatable.'.$entity);

		$output = array();

		foreach ($config as $field => $data)
		{
			$row = NULL;

			if (arr::get($data, 'show') === TRUE)
			{
				$row = '{"mDataProp": "'.$field.'"';

				if ($width = arr::get($data, 'width'))
				{
					$row .= ', "sWidth": "'.$width.'"';
				}

				$row .= '}';
			}

			$row AND $output[] = $row;
		}

		$render AND $output = implode(','.PHP_EOL, $output);

		return $output;
	}

	public static function unsortable($entity, $render = FALSE)
	{
		$config = Kohana::config('datatable.'.$entity);

		$output = array();

		// indici dei campi di partenza
		$keys = array_keys($config);

		// filtra i campi
		$selected = array_filter($config, function($data) {
			return arr::get($data, 'show') === TRUE AND arr::get($data, 'sortable') === FALSE;
		});

		// seleziona gli indici sulla base delle chiavi rimaste
		$output = array_map(function($field) use ($keys) {
			return array_search($field, $keys);
		}, array_keys($selected));

		$render AND $output = '{"bSortable": false, "aTargets": ['
			.implode(',', $output)
			.']}';

		return $output;
	}

	public static function add_extra_col($entity, array $column = NULL)
	{
		if ( ! $column)
			return;

		$key = key($column);

		$data = arr::merge(array(
			// 'column'   => '',
			'show'     => TRUE,
			'heading'  => '',
			'class'    => '',
			'style'    => '',
			'width'    => '',
			'sortable' => TRUE,
		), $column[$key]);

		$current_settings = Kohana::config('datatable.'.$entity);

		Kohana::config_set('datatable.'.$entity, arr::merge($current_settings, array($key => $data)));
	}

} // End dt