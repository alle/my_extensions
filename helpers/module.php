<?php defined('SYSPATH') OR die('No direct access allowed.');

class module_Core {

	private static $modules = array();

	public static function check($module)
	{
		self::_store_modules();

		return (bool) strpos(implode(',', self::$modules).',', '/'.$module.',');
	}

	private static function _store_modules()
	{
		if (count(self::$modules) === 0)
		{
			self::$modules = Kohana::config('core.modules');
		}
	}

} // End module_Core