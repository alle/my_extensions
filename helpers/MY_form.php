<?php defined('SYSPATH') OR die('No direct access allowed.');

class form extends form_Core {

	public static function hidden($data, $value = '', $extra = '')
	{
		if ( ! is_array($data))
		{
			$data = array
			(
				$data => $value
			);
		}

		$input = '';
		foreach ($data as $name => $value)
		{
			$attr = array
				(
				'type' => 'hidden',
				'name' => $name,
				'id' => $name,
				'value' => $value
			);

			$input .= form::input($attr, NULL, $extra)."\n";
		}

		return $input;
	}

} // End form