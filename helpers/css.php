<?php defined('SYSPATH') OR die('No direct access allowed.');

class css_Core {

	static protected $css = array();

	public static function add($files = array())
	{
		if (is_string($files))
		{
			$files = array($files => FALSE);
		}

		foreach ($files as $file => $media)
		{
			if (is_int($file))
			{
				self::$css[$media] = FALSE;
			}
			else
			{
				self::$css[$file] = $media;
			}
		}
	}

	public static function remove($files = array())
	{
		if (is_string($files))
		{
			$files = array($files);
		}

		foreach (self::$css as $key => $file)
		{
			if (array_key_exists($file, self::$css))
			{
				unset(self::$css[$file]);
			}
		}
	}

	static public function render($print = FALSE)
	{
		$output = '';
		foreach (self::$css as $file => $media)
		{
			$output .= html::stylesheet($file, $media);
		}

		if ($print)
		{
			echo $output;
		}

		return $output;
	}

} // End css_Core