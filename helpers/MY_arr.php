<?php defined('SYSPATH') OR die('No direct access allowed.');

class arr extends arr_Core {

	public static $delimiter = '.';

	public static function clear($array)
	{
		foreach($array as $k => $v)
		{
			$array[$k] = null;
		}
		return $array;
	}

	public static function recursive_in_array($needle = NULL, $haystack = array())
	{
		if ( ! empty($needle))
		{
			foreach ($haystack as $stalk) {
				if ($needle == $stalk OR (is_array($stalk) AND arr::recursive_in_array($needle, $stalk))) {
					return TRUE;
				}
			}
		}
		return FALSE;
	}

	public static function recursive_array_search($needle, $haystack, $index = NULL)
	{
		$aIt = new RecursiveArrayIterator($haystack);
		$it = new RecursiveIteratorIterator($aIt);

		while ($it->valid())
		{
			if (((isset($index) AND ($it->key() == $index))
				OR ( ! isset($index))) AND ($it->current() == $needle))
			{
				return $aIt->key();
			}

			$it->next();
		}

		return FALSE;
	}

	/**
	 * Check if multiple keys exist in a given array and return false if any of them don't
	 * or the minimum specified number of keys exist
	 *
	 * @param array    array of keys to search for
	 * @param array    array to search on
	 * @param bool     min number of keys to be present in the keys array (TRUE, FALSE, 0 = all keys must exist)
	 * @return bool
	 */
	public static function array_keys_exist($keys = array(), $array = array(), $min_keys = NULL)
	{
		if (is_string($keys))
		{
			$keys = array($keys);
		}

		if ((is_bool($min_keys) AND $min_keys) OR ((int) $min_keys == 0))
		{
			$min_keys = count($keys);
		}

		if (count(array_intersect($keys, array_keys($array))) >= $min_keys)
		{
			return TRUE;
		}
		return FALSE;
	}

	public static function string_to_array($string = NULL)
	{
		if (empty($string))
			return array();

		return explode(',', $string);
	}

	/**
	 * Riduce la lunghezza di un array monodimensionale ad una nuova lunghezza
	 * @param array $array array da ridurre
	 * @param int $length dimensione finale
	 * @return array
	 */
	public static function trim(array $array, $length)
	{
		$new_array = array();
		$length = min($length, count($array));

		for($i = 0; $i < $length; $i++)
		{
			array_push($new_array, $array[$i]);
		}

		return (array) $new_array;
	}

	/**
	 * Sposta su un elemento dell'array
	 * @param array $input L'array da modificarre
	 * @param int $index L'indice dell'elemento da spostare su
	 * @return array
	 */
	public static function move_up(array $input, $index)
	{
		$new_array = $input;

		if (count($input) > $index AND $index > 0)
		{
			array_splice($new_array, $index - 1, 0, $input[$index]);
			array_splice($new_array, $index + 1, 1);
		}

		return $new_array;
	}

	/**
	 * Sposta giù un elemento dell'array
	 * @param array $input L'array da modificarre
	 * @param int $index L'indice dell'elemento da spostare giù
	 * @return array
	 */
	public static function move_down(array $input, $index)
	{
		$new_array = $input;

		if (count($input) > $index) {
			array_splice($new_array, $index + 2, 0, $input[$index]);
			array_splice($new_array, $index, 1);
		}

		return $new_array;
	}

	/**
	 * Retrieve a single key from an array. If the key does not exist in the
	 * array, the default value will be returned instead.
	 *
	 *     // Get the value "username" from $_POST, if it exists
	 *     $username = Arr::get($_POST, 'username');
	 *
	 *     // Get the value "sorting" from $_GET, if it exists
	 *     $sorting = Arr::get($_GET, 'sorting');
	 *
	 * @param   array   array to extract from
	 * @param   string  key name
	 * @param   mixed   default value
	 * @return  mixed
	 */
	public static function get($array, $key, $default = NULL)
	{
		return isset($array[$key]) ? $array[$key] : $default;
	}




	/*
	 * dall'estensione 'helpers'
	 */



	/**
	* Creates a deep and unreferenced copy of an array
	*
	* @param
	* @param
	*/
	public static function deep_copy (&$arr, &$copy)
	{
		// checks
			// return if NULL
				if($arr == NULL)return false;

			// create empty array if copy is null
				if(!is_array($copy))$copy = array();

		// copy keys
			foreach($arr as $k => $v)
			{
				if(is_array($v))
				{
					array_deep_copy($v, $copy[$k]);
				}
				else
				{
					$copy[$k] = $v;
				}
			}
	}


	/**
	* Filters an existing array by key name to return an array with only those keys
	* that match the criteria (regular expression or stringcomparison)
	*
	* @param
	* @param
	*/
	public static function filter_by_key($arr_in, $obj)
	{
		$arr_out	= array();

		// if a regular expression is handed in
			if(is_string($obj) && substr($obj, 0, 1) == '/' && substr($obj, -1, 1) == '/')
			{
				foreach($arr_in as $k => $v)
				{
					if(preg_match($obj, $k))
					{
						$arr_out[$k] = $v;
					}
				}
			}

		// if is an array, check the key names against the array
			else if(is_array($obj))
			{
				foreach($arr_in as $k => $v)
				{
					if(in_array($k, $obj))
					{
						$arr_out[$k] = $v;
					}
				}
			}

		// if not do a straight string comparison
			else
			{
				foreach($arr_in as $k => $v)
				{
					if(strstr($k, $obj) !== FALSE)
					{
						$arr_out[$k] = $v;
					}
				}
			}

		// return
			return $arr_out;
	}


	/**
	* Reorder an array according to a subsequent key-order you specify
 	* Key order can be a numeric or associative array, or most simply a comma-delimeted string
	*
	* @param	arr_in		The input array
	* @param	$keys		The
	* @param
	* @param
	*/
	public static function reorder_by_key($arr_in, $keys, $matches_only = false)
	{

		// variables

			// key keys from a string
				if(is_string($keys))
				{
					if(function_exists($keys))
					{
						$fn				= $keys;
						$keys			= NULL;
						$matches_only	= TRUE;
					}
					else
					{
						$keys = preg_split('%,\s*%', $keys);
					}
				}
				else
				{
					// should be be an array
					// print_r(array_values(array_keys($arr)));
				}

			// output array
				$arr_out	= array();

		// process

			// if keys are provided, extract correct keys from input array
				if($keys)
				{
					foreach($keys as $k)
					{
						if($arr_in[$k])
						{
							$arr_out[$k] = $arr_in[$k];
							unset($arr_in[$k]);
						}
					}
				}

			// if a function is provided, get the key indexes via the function
				elseif($fn)
				{
					// index cache
						$index_cache = array();

					// loop through the input array's keys derive each one's index via the sort function
						foreach($arr_in as $key => $value)
						{
							$index	= $fn($key);
							$index_cache[$index] = $key;
						}

					// sort the indices, then loop through and build final array
						ksort($index_cache);
						foreach($index_cache as $index => $key)
						{
							$arr_out[$key] = $arr_in[$key];
						}
				}

		// return

			// return reordered array, plus remaining keys
				return $matches_only ? $arr_out : array_merge($arr_out, $arr_in);

	}


	/**
	* Transposes an array.
	*
	* Imagine taking a 10x3 table and swapping rows and columns to return a 3x10 table
	*
	* @param		a single array, or a list of arrays as multiple arguments
	*/
	public static function transpose()
	{

		// variables
			$input_arrays	= func_get_args();
			$output_rows	= array();

		// if a single (associative) array is passed, grab it into the top-level array
			$input_rows		= count($input_arrays) == 1 ? $input_arrays[0] : $input_arrays;

		// loop left-to-right through the columns (by colIndex)
			$col_index = 0;
			do
			{
				// new array to grab current column values
					$cur_col = array();

				// loop top-to-bottom thorugh rows of current column (by rowName)
					foreach($input_rows as $row_name => $row_values)
					{
						$col_value			= $row_values[$col_index];
						$cur_col[$row_name]	= $col_value;
					}

				// add curCol to outputRows if values existed in current column
					if($col_value !== NULL)
					{
						array_push($output_rows, $cur_col);
						$col_index++;
					}
			}
			while($col_value !== NULL);

		// return
			return $output_rows;
	}


	/**
	* Convert a numeric array of words to an associative array, optionally converting
	* both the keys and values to different cases. Useful for things like converting a
	* bunch of options to "key name" and "Value name"
	*
	* @param	arr_in					a single numeric array
	* @param	values_to_sentence		create values in Sentence case
	* @param	keys_to_lower			create keys in lower case
	*/
	public static function to_associative($arr_in, $values_to_sentence = TRUE, $keys_to_lower = TRUE)
	{
		$arr_out = array();
		foreach($arr_in as $val)
		{
			$arr_out[$keys_to_lower ? strtolower($val) : $val] = str_replace('_', ' ', $values_to_sentence ? ucwords($val) : $val);
		}
		return $arr_out;
	}



	/**
	* Exports an array to nicely-formatted, parsable, PHP code
	*
	* echo arr::export(array(1,2,3, 'test' => array(1,2,3)), 'config');
	*/
	public static function export($arr, $name = 'config', $php_tags = TRUE)
	{
		// anonymous functions
			$uncuddle_brackets	= create_function('$matches', '$indent = substr($matches[1], 1); return "array\n$indent(\n$indent	";');
			$cuddle_array		= create_function('$matches', 'return "=> array";');

		// prettify output
			$str = var_export($arr, TRUE);
			$str = str_replace('  ', '	', $str);
			$str = preg_replace('/\d+ => [\r\n]?\t?/', '', $str);
			$str = preg_replace_callback('/array \([\r\n]+(\t+)/', $uncuddle_brackets, $str);
			$str = preg_replace_callback('/=>\s+array/', $cuddle_array, $str);

		// add name
			$str = '$' . $name . ' = ' . $str .';';

		// add tags
			if($php_tags)
			{
				$str = "<?php\n" . $str . "\n?>";
			}

		// return
			return $str;
	}

	// da Kohana 3.2.0

	/**
	 * Tests if an array is associative or not.
	 *
	 *     // Returns TRUE
	 *     Arr::is_assoc(array('username' => 'john.doe'));
	 *
	 *     // Returns FALSE
	 *     Arr::is_assoc('foo', 'bar');
	 *
	 * @param   array   array to check
	 * @return  boolean
	 */
	public static function is_assoc(array $array)
	{
		// Keys of the array
		$keys = array_keys($array);

		// If the array keys of the keys match the keys, then the array must
		// not be associative (e.g. the keys array looked like {0:0, 1:1...}).
		return array_keys($keys) !== $keys;
	}

	/**
	 * Test if a value is an array with an additional check for array-like objects.
	 *
	 *     // Returns TRUE
	 *     Arr::is_array(array());
	 *     Arr::is_array(new ArrayObject);
	 *
	 *     // Returns FALSE
	 *     Arr::is_array(FALSE);
	 *     Arr::is_array('not an array!');
	 *     Arr::is_array(Database::instance());
	 *
	 * @param   mixed    value to check
	 * @return  boolean
	 */
	public static function is_array($value)
	{
		if (is_array($value))
		{
			// Definitely an array
			return TRUE;
		}
		else
		{
			// Possibly a Traversable object, functionally the same as an array
			return (is_object($value) AND $value instanceof Traversable);
		}
	}

	/**
	 * Gets a value from an array using a dot separated path.
	 *
	 *     // Get the value of $array['foo']['bar']
	 *     $value = Arr::path($array, 'foo.bar');
	 *
	 * Using a wildcard "*" will search intermediate arrays and return an array.
	 *
	 *     // Get the values of "color" in theme
	 *     $colors = Arr::path($array, 'theme.*.color');
	 *
	 *     // Using an array of keys
	 *     $colors = Arr::path($array, array('theme', '*', 'color'));
	 *
	 * @param   array   array to search
	 * @param   mixed   key path string (delimiter separated) or array of keys
	 * @param   mixed   default value if the path is not set
	 * @param   string  key path delimiter
	 * @return  mixed
	 */
	public static function path($array, $path, $default = NULL, $delimiter = NULL)
	{
		if ( ! arr::is_array($array))
		{
			// This is not an array!
			return $default;
		}

		if (is_array($path))
		{
			// The path has already been separated into keys
			$keys = $path;
		}
		else
		{
			if (array_key_exists($path, $array))
			{
				// No need to do extra processing
				return $array[$path];
			}

			if ($delimiter === NULL)
			{
				// Use the default delimiter
				$delimiter = arr::$delimiter;
			}

			// Remove starting delimiters and spaces
			$path = ltrim($path, "{$delimiter} ");

			// Remove ending delimiters, spaces, and wildcards
			$path = rtrim($path, "{$delimiter} *");

			// Split the keys by delimiter
			$keys = explode($delimiter, $path);
		}

		do
		{
			$key = array_shift($keys);

			if (ctype_digit($key))
			{
				// Make the key an integer
				$key = (int) $key;
			}

			if (isset($array[$key]))
			{
				if ($keys)
				{
					if (arr::is_array($array[$key]))
					{
						// Dig down into the next part of the path
						$array = $array[$key];
					}
					else
					{
						// Unable to dig deeper
						break;
					}
				}
				else
				{
					// Found the path requested
					return $array[$key];
				}
			}
			elseif ($key === '*')
			{
				// Handle wildcards

				$values = array();
				foreach ($array as $arr)
				{
					if ($value = arr::path($arr, implode('.', $keys)))
					{
						$values[] = $value;
					}
				}

				if ($values)
				{
					// Found the values requested
					return $values;
				}
				else
				{
					// Unable to dig deeper
					break;
				}
			}
			else
			{
				// Unable to dig deeper
				break;
			}
		}
		while ($keys);

		// Unable to find the value requested
		return $default;
	}

	/**
	* Set a value on an array by path.
	*
	* @see Arr::path()
	* @param array   $array     Array to update
	* @param string  $path      Path
	* @param mixed   $value     Value to set
	* @param string  $delimiter Path delimiter
	*/
	public static function set_path( & $array, $path, $value, $delimiter = NULL)
	{
		if ( ! $delimiter)
		{
			// Use the default delimiter
			$delimiter = arr::$delimiter;
		}

		// Split the keys by delimiter
		$keys = explode($delimiter, $path);

		// Set current $array to inner-most array path
		while (count($keys) > 1)
		{
			$key = array_shift($keys);

			if (ctype_digit($key))
			{
				// Make the key an integer
				$key = (int) $key;
			}

			if ( ! isset($array[$key]))
			{
				$array[$key] = array();
			}

			$array = & $array[$key];
		}

		// Set key on inner-most array
		$array[array_shift($keys)] = $value;
	}

	/**
	 * Retrieves muliple single-key values from a list of arrays.
	 *
	 *     // Get all of the "id" values from a result
	 *     $ids = Arr::pluck($result, 'id');
	 *
	 * [!!] A list of arrays is an array that contains arrays, eg: array(array $a, array $b, array $c, ...)
	 *
	 * @param   array   list of arrays to check
	 * @param   string  key to pluck
	 * @return  array
	 */
	public static function pluck($array, $key)
	{
		$values = array();

		foreach ($array as $row)
		{
			if (isset($row[$key]))
			{
				// Found a value in this row
				$values[] = $row[$key];
			}
		}

		return $values;
	}

} // End arr