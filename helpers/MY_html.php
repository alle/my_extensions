<?php defined('SYSPATH') OR die('No direct access allowed.');

class html extends html_Core {

	/**
	 * Creates a script link.
	 *
	 * @param   string|array  filename
	 * @param   boolean       include the index_page in the link
	 * @return  string
	 */
	public static function script($script, $index = FALSE, $unique = FALSE, $unique_name = 'time')
	{
		$compiled = '';

		if (is_array($script))
		{
			foreach ($script as $name)
			{
				$compiled .= html::script($name, $index, $unique, $unique_name);
			}
		}
		else
		{
			if (strpos($script, '://') === FALSE)
			{
				// Add the suffix only when it's not already present
				$script = url::base((bool) $index).$script;
			}

			if (substr_compare($script, '.js', -3, 3, FALSE) !== 0)
			{
				// Add the javascript suffix
				$script .= '.js';
			}

			if ($unique)
			{
				$script .= '?'.$unique_name.'='.$unique;
			}

			$compiled = '<script type="text/javascript" src="'.$script.'"></script>';
		}

		return $compiled."\n";
	}

	public static function sanitize($string)
	{
		return preg_replace('/[^a-zA-Z0-9:_.-]/', '_', $string);
	}

} // End html
