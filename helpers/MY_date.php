<?php defined('SYSPATH') or die('No direct access allowed.');

class date extends date_Core {

	public static function today()
	{
		return mktime(0, 0, 0, date('m'), date('d'), date('y'));
	}

	public static function datepicker_to_sqldate($date)
	{
		$matches = preg_split('/\//', $date);

		return sprintf('%s-%s-%s', $matches[2], $matches[1], $matches[0]);
	}

	public static function sqldate_to_datepicker($date)
	{
		$matches = preg_split('/\-/', $date);

		return sprintf('%s/%s/%s', $matches[2], $matches[1], $matches[0]);
	}

	public static function expire_days_to_text($days, $prefix = '')
	{
		$days = (int) $days;

		if ($days >= -2 AND $days <= 2)
			return $prefix.Kohana::lang('days_to_text.'.$days);

		return $prefix.(($days < 0)
			? Kohana::lang('days_to_text.in', abs($days))
			: Kohana::lang('days_to_text.from', $days));
	}

} // End date
