<?php defined('SYSPATH') OR die('No direct access allowed.');

class seo_Core {

	static protected $ga;
	static protected $verification = array();

	public static function set_ga_code($code = '')
	{
		self::$ga = $code
			? $code
			: Kohana::config('seo.ga');
	}

	static public function ga_script($print = FALSE, $template = '')
	{
		$output = '';

		self::$ga OR self::set_ga_code();

		if (self::$ga)
		{
			$template OR $template = Kohana::config('seo.template');

			$output = new View($template);
			$output->ga = self::$ga;
		}

		if ($print)
		{
			echo $output;
		}

		return $output;
	}

	public static function add_verification_code($engine = '', $code = '')
	{
		self::$verification OR self::$verification = Kohana::config('seo.verification');

		if (is_string($engine) AND $engine)
		{
			if ($code)
			{
				self::$verification[$engine] = $code;
			}
			else
			{
				unset(self::$verification[$engine]);
			}
		}
	}

	static public function meta_verification($engine, $print = FALSE)
	{
		$output = '';

		if (isset(self::$verification[$engine]))
		{
			$output = html::meta($engine, self::$verification[$engine])."\n";
		}

		if ($print)
		{
			echo $output;
		}

		return $output;
	}

	static public function meta_verification_all($print = FALSE)
	{
		$output = '';

		self::$verification OR self::add_verification_code();

		foreach (self::$verification as $engine => $code)
		{
			$output .= self::meta_verification($engine, FALSE);
		}

		if ($print)
		{
			echo $output;
		}

		return $output;
	}

	static public function set_domain_name($print = FALSE)
	{
		$output = '';

		if ($domain_name = Kohana::config('seo.setDomainName'))
		{
			$domain_name = '.'.ltrim($domain_name, '.');

			$output = sprintf('_gaq.push([\'_setDomainName\', \'%s\']);', $domain_name);
			$output .= ' _gaq.push([\'_setAllowHash\', false]);';
			$output .= "\n";
		}

		if ($print)
			echo $output;

		return $output;
	}

} // End seo_Core