<?php defined('SYSPATH') OR die('No direct access allowed.');

class lang_Core {

	public static function get($key, $args = array())
	{
		return Kohana::lang($key, $args);
	}

} // End config