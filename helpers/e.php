<?php defined('SYSPATH') OR die('No direct access allowed.');

class e_Core {

	public static function cr()
	{
		return ((strtolower(PHP_SAPI) === 'cli') ? '' : '<br />').PHP_EOL;
	}


	public static function log($msg = '', $echo = TRUE)
	{
		$output = date('Y-m-d H:i:s P').' --- '.$msg.(php_sapi_name() !== 'cli' ? '<br />' : '').PHP_EOL;

		if ($echo !== TRUE)
			return $output;

		echo $output;
		if (ob_get_contents())
		{
			ob_flush();
			flush();
		}
	}

} // End e_Core