<?php defined('SYSPATH') OR die('No direct script access.');

class filesystem extends filesystem_Core {

	/**
	 * Map
	 *
	 * Recursively gets all files and folders in the directory, with an optional depth limit
	 *
	 * @param	string	the path to the folder
	 * @param	number	how many levels to process
	 * @return	array	The list of files and folders
	 */

	public static function map($path, $levels = NULL, $structured = FALSE, $files_first = FALSE)
	{
		// trim trailing slashes
			$levels		= is_null($levels) ? -1 : $levels;
			$path		= preg_replace('|/+$|', '', $path);

		// filesystem objects
			$files		= array();
			$folders	= array();

			if ( ! is_dir($path) OR ($scandir = scandir($path)) === FALSE)
			{
				$scandir = array();
			}

			$objects	= array_diff($scandir, array('.', '..'));

		// check through
			foreach($objects as $v)
			{
				$dir = $path . '/' .$v;
				if(is_dir($dir))
				{
					$folders[$v] = $levels != 0 ? filesystem::map($dir, $levels - 1, $structured, $files_first) : $v;
				}
				else
				{
					array_push($files, $v);
				}
			}

		// return
			if($structured)
			{
				return array('/folders' => $folders, '/files' => $files);
			}
			else
			{
				return $files_first ? array_merge($files, $folders) : array_merge($folders, $files);
			}
	}

	/**
	 * Get files
	 *
	 * Returns all files in the directory with an optional regexp OR file extension mask
	 *
	 * @param	string	path to the folder
	 * @param	string	Regular expression or file extension to limit the search to
	 * @param	string	Append the initial path to the return values
	 * @return	array	The list of files
	 */

	//print_r(filesystem::get_files('/', array('ico', 'xml')));
	//print_r(filesystem::get_files('/', '/(\.ico|\.xml)$/'));
	//print_r(filesystem::get_files('/'));

	public static function get_files($path, $mask = NULL, $appendPath = false)
	{
		// objects
			$files		= array();
			$path		= preg_replace('%/+$%', '/', $path . '/'); // add trailing slash

			if ( ! is_dir($path) OR ($scandir = scandir($path)) === FALSE)
			{
				$scandir = array();
			}

			$objects	= array_diff($scandir, array('.', '..'));

		// mask
			if($mask != NULL)
			{
				// regular expression for detecing a regular expression
					$rxIsRegExp	= '/^([%|\/]|{).+(\1|})[imsxeADSUXJu]*$/';

				// an array of file extenstions
					if(is_array($mask))
					{
						$mask = '%\.(' .implode('|', $mask). ')$%i';
					}

				// if the supplied mask is NOT a regular expression...
				// assume it's a file extension and make it a regular expression
					else if(!preg_match($rxIsRegExp, $mask))
					{
						$mask = "/\.$mask$/i";
					}
			}


		// match
			foreach($objects as $object)
			{
				if(is_file($path.$object) && ($mask != NULL ? preg_match($mask, $object) : TRUE))
				{
					array_push($files, $appendPath ? $path.$object : $object);
				}
			}

		// return
			return $files;
	}

	public static function copy_r($path, $dest, $permissions = 0755)
	{
		if (is_dir($path))
		{
			@mkdir($dest, $permissions);

			$objects = scandir($path);

			if (sizeof($objects) > 0)
			{
				foreach ($objects as $file)
				{
					if ($file == '.' || $file == '..')
						continue;

					if (is_dir($path.DIRECTORY_SEPARATOR.$file))
					{
						copy_r($path.DIRECTORY_SEPARATOR.$file, $dest.DIRECTORY_SEPARATOR.$file);
					}
					else
					{
						copy($path.DIRECTORY_SEPARATOR.$file, $dest.DIRECTORY_SEPARATOR.$file);
					}
				}
			}

			return true;
		}
		elseif (is_file($path))
		{
			return copy($path, $dest);
		}
		else
		{
			return false;
		}
	}

} // End filesystem