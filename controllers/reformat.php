<?php defined('SYSPATH') OR die('No direct access allowed.');

class reformat_Controller extends Controller {

	const ALLOW_PRODUCTION = FALSE;

	function xml($input, $output = '')
	{
		$file = file_get_contents($input);
		$xml = new SimpleXMLExtended($file);
		$output = ($output) ? $output : $input;
		$xml->asXML($output);
	}

}