<?php
/**
* Auto_Modeler_ORM
*
* @package        Auto_Modeler
* @author         Jeremy Bush
* @copyright      (c) 2008 Jeremy Bush
* @license        http://www.opensource.org/licenses/isc-license.txt
*/

class Auto_Modeler_ORM extends Auto_Modeler_ORM_Core
{

	public function join_table($table)
	{
		if ($this->table_name > $table)
		{
			$table = $table.'_'.$this->table_name;
		}
		else
		{
			$table = $this->table_name.'_'.$table;
		}

		return $table;
	}

	// Value is an ID
	/*
	public function has($key, $value)
	{
		$original_key = $key;
		if (isset($this->aliases[$key]))
			$key = $this->aliases[$key];
		$table_name = isset($this->aliases[$this->table_name]) ? $this->aliases[$this->table_name] : $this->table_name;

		$join_table = $this->join_table(inflector::plural($key)); // $table_name.'_'.$key;
		$f_key = inflector::singular($original_key).'_id';
		$this_key = inflector::singular($table_name).'_id';

		if (in_array($original_key, $this->has_many))
		{
			return (bool) $this->db->select($key.'.id')->from($key)->where(array($join_table.'.'.$this_key => $this->data['id'], $join_table.'.'.$f_key => $value))->join($join_table, $join_table.'.'.$f_key, $key.'.id')->get()->count();
		}
		return FALSE;
	}
	*/

}