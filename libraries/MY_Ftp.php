<?php defined('SYSPATH') OR die('No direct script access.');

class Ftp extends Ftp_Core {

	public function passive($passive = TRUE)
	{
		return (bool) ftp_pasv($this->connection, $passive);
	}

	/**
	 * Put executes a put command on the remote
	 * FTP server.
	 *
	 * @param	varchar $local
	 * @param	varchar $remote
	 * @param	const	$mode
	 * @return	boolean
	 */
	public function put($local, $remote, $mode = FTP_BINARY)
	{
		// Upload the local file to the remote location specified
		// override funzione di libreria per remote e local invertiti
		if (ftp_put($this->connection, $remote, $local, $mode))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

}