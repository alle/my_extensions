<?php defined('SYSPATH') OR die('No direct script access.');

class Model extends Model_Core {

	public function table_name()
	{
		return isset($this->table_name) ? $this->table_name : FALSE;
	}

}