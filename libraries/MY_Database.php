<?php defined('SYSPATH') OR die('No direct script access.');

class Database extends Database_Core {

	public function nat_orderby($orderby, $direction = NULL)
	{
		if ( ! is_array($orderby))
		{
			$orderby = array($orderby => $direction);
		}

		foreach ($orderby as $column => $direction)
		{
			$direction = strtoupper(trim($direction));

			// Add a direction if the provided one isn't valid
			if ( ! in_array($direction, array('ASC', 'DESC', 'RAND()', 'RANDOM()', 'NULL')))
			{
				$direction = 'ASC';
			}

			// Add the table prefix if a table.column was passed
			if (strpos($column, '.'))
			{
				$column = $this->config['table_prefix'].$column;
			}

			// $this->orderby[] = $this->driver->escape_column($column).' '.$direction;
			// $this->orderby[] = $this->driver->escape_column($column).' + 0 '.$direction;
			$this->orderby[] = 'LENGTH('.$this->driver->escape_column($column).') '.$direction
				.', '.$this->driver->escape_column($column).' '.$direction;
		}

		return $this;
	}

	/**
	 * Opens a parenthesis in the where clause
	 *
	 * @return Database
	 */
	public function open()
	{
		$this->where[] = '(';
		return $this;
	}

	/**
	 * Closes a parenthesis in the where clause
	 *
	 * @return Database
	 */
	public function close()
	{
		// Search backwards for the last opening paren and resolve it
		$i = count($this->where) - 1;
		$this->where[$i] .= ')';

		while (--$i >= 0)
		{
			if ($this->where[$i] == '(')
			{
				// Remove the paren from the where clauses, and add it to the right of the operator of the
				// next where clause.  If removing the paren makes the next where clause the first element
				// in the where list, then the operator shouldn't be there.  It's there because we
				// calculate whether or not we need an operator based on the number of where clauses, and
				// the open paren seems like a where clause even though it isn't.
				array_splice($this->where, $i, 1);
				$this->where[$i] = preg_replace("/^(AND|OR) /", $i ? "\\1 (" : "(", $this->where[$i]);

				return $this;
			}
		}

		// An error if we get to this point...
		return $this;
	}

	/**
	 * Adds an "OR" .. "IN" condition to the where clause
	 *
	 * @param   string  Name of the column being examined
	 * @param   mixed   An array or string to match against
	 * @param   bool    Generate a NOT IN clause instead
	 * @return  Database_Core  This Database object.
	 */
	public function orin($field, $values, $not = FALSE)
	{
		if (is_array($values))
		{
			$escaped_values = array();
			foreach ($values as $v)
			{
				if (is_numeric($v))
				{
					$escaped_values[] = $v;
				}
				else
				{
					$escaped_values[] = "'".$this->driver->escape_str($v)."'";
				}
			}
			$values = implode(",", $escaped_values);
		}

		$where = $this->driver->escape_column(((strpos($field,'.') !== FALSE) ? $this->config['table_prefix'] : ''). $field).' '.($not === TRUE ? 'NOT ' : '').'IN ('.$values.')';
		$this->where[] = $this->driver->where($where, '', 'OR ', count($this->where), -1);

		return $this;
	}

	/**
	 * Generates the JOIN portion of the query.
	 *
	 * @param   string        table name
	 * @param   string|array  where key or array of key => value pairs
	 * @param   string        where value
	 * @param   string        type of join
	 * @return  Database_Core        This Database object.
	 */
	public function join($table, $key, $value = NULL, $type = '')
	{
		$join = array();

		if ( ! empty($type))
		{
			$type = strtoupper(trim($type));

			if ( ! in_array($type, array('LEFT', 'RIGHT', 'OUTER', 'INNER', 'LEFT OUTER', 'RIGHT OUTER'), TRUE))
			{
				$type = '';
			}
			else
			{
				$type .= ' ';
			}
		}

		$cond = array();
		$keys  = is_array($key) ? $key : array($key => $value);
		foreach ($keys as $key => $value)
		{
			$key    = (strpos($key, '.') !== FALSE) ? $this->config['table_prefix'].$key : $key;

			if (is_string($value))
			{
				// Only escape if it's a string
				$value = $this->driver->escape_column($this->config['table_prefix'].$value);
			}
			elseif (is_array($value))
			{
				if (strpos($value[0], '(') !== FALSE)
				{
					$value = new Database_Expression($value[0]);
				}
				else
				{
					$value = array_map(function($_value) {
						if (is_string($_value)) {
							$_value = trim($_value, "'");
							$_value = "'$_value'";
						}
						return $_value;
					}, $value);

					$value = implode(',', $value);
				}

				$cond[] = $this->driver->where($key.' IN ('.$value.')', $value, 'AND ', count($cond), -1);
				continue;
			}

			$cond[] = $this->driver->where($key, $value, 'AND ', count($cond), FALSE);
		}

		if ( ! is_array($this->join))
		{
			$this->join = array();
		}

		if ( ! is_array($table))
		{
			$table = array($table);
		}

		foreach ($table as $t)
		{
			if (is_string($t))
			{
				// TODO: Temporary solution, this should be moved to database driver (AS is checked for twice)
				if (stripos($t, ' AS ') !== FALSE)
				{
					$t = str_ireplace(' AS ', ' AS ', $t);

					list($table, $alias) = explode(' AS ', $t);

					// Attach prefix to both sides of the AS
					$t = $this->config['table_prefix'].$table.' AS '.$this->config['table_prefix'].$alias;
				}
				else
				{
					$t = $this->config['table_prefix'].$t;
				}
			}

			$join['tables'][] = $this->driver->escape_column($t);
		}

		$join['conditions'] = '('.trim(implode(' ', $cond)).')';
		$join['type'] = $type;

		$this->join[] = $join;

		return $this;
	}

	/**
	 * Transaction status variable
	 *
	 * @var in_transaction
	 */
	protected $in_transaction = FALSE;

	public function __destruct()
	{
		self::trans_rollback();
	}

	/**
	 * Status of transaction
	 *
	 * @return  void
	 */
	public function trans_status()
	{
		return $this->in_transaction;
	}

	/**
	 * Start a transaction
	 *
	 * @return  void
	 */
	public function trans_begin()
	{
		if ($this->in_transaction === FALSE)
		{
			$this->query('SET AUTOCOMMIT=0');
			$this->query('BEGIN');
		}
		$this->in_transaction = TRUE;
	}

	/**
	 * Finish the transaction
	 *
	 * @return  void
	 */
	public function trans_commit()
	{
		if ($this->in_transaction === TRUE)
		{
			$this->query('COMMIT');
			$this->query('SET AUTOCOMMIT=1');
		}
		$this->in_transaction = FALSE;
	}

	/**
	 * Undo the transaction
	 *
	 * @return  void
	 */
	public function trans_rollback()
	{
		if ($this->in_transaction === TRUE)
		{
			$this->query('ROLLBACK');
			$this->query('SET AUTOCOMMIT=1');
		}
		$this->in_transaction = FALSE;
	}

	public function get_property($name)
	{
		if (isset($this->$name))
			return $this->$name;

		return NULL;
	}

	public function reset_query_select()
	{
		$this->reset_select();
	}

}