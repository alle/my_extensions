<?php defined('SYSPATH') OR die('No direct access allowed.');

class MultiUploadValidation {

	protected $array = array();
	protected $errors = array();
	protected $field;
	protected $rules;

	public static function factory(array $array, $field)
	{
		return new MultiUploadValidation($array, $field);
	}

	public function __construct(array $array, $field)
	{
		$this->array[$field] = arr::rotate($array[$field]);
		$this->field = $field;
	}

	public function set_rules($rules)
	{
		$this->rules = func_get_args();
	}

	public function validate($errors_file = NULL)
	{
		$field = $this->field;
		$array = $this->array;
		$rules = $this->rules;
		$result = TRUE;

		foreach ($array[$field] as $key => $f)
		{
			$g[$field] = $f;
			$validations[$key] = Validation::factory($g);

			foreach ($this->rules as $rule)
			{
				$validations[$key]->add_rules($field, $rule);
			}

			$temp = $validations[$key]->validate();
			$result = $result && $temp;
			$this->errors[$key] = $validations[$key]->errors($errors_file);
		}

		return $result;
	}

	public function errors()
	{
		return $this->errors;
	}

} // End MultiUploadValidation