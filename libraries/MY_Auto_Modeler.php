<?php defined('SYSPATH') OR die('No direct access allowed.');

class Auto_Modeler extends Auto_Modeler_Core {

	protected $primary_key = 'id';

	public function __isset($key)
	{
		return isset($this->data[$key]);
	}

	public function __unset($key)
	{
		unset($this->data[$key]);
	}

	public function fetch_all($order_by = 'id', $direction = 'ASC')
	{
		return $this->db->orderby($order_by, $direction)->get($this->table_name);
	}

	public function fetch_where($where = array(), $order_by = 'id', $direction = 'ASC', $type = '')
	{
		$function = $type.'where';
		return $this->db->$function($where)->orderby($order_by, $direction)->get($this->table_name);
	}

	public function select_list($key, $display, $order_by = 'id', $where = array())
	{
		$query = empty($where) ? $this->fetch_all($order_by) : $this->db->where($where)->orderby($order_by)->get($this->table_name);

		return Auto_Modeler::_select_rows($query, $key, $display);
	}

	protected static function _select_rows($query, $key, $display, $rows = array())
	{
		foreach ($query as $row)
		{
			if (is_array($display))
			{
				$display_str = array();

				foreach ($display as $text)
				{
					$display_str[] = $row->$text;
				}

				$rows[$row->$key] = implode(' - ', $display_str);
			}
			else
			{
				$rows[$row->$key] = $row->$display;
			}
		}

		return $rows;
	}

	public function last_query()
	{
		return $this->db->last_query();
	}

	public function get_fields()
	{
		return $this->data;
	}

	public function get_field_names()
	{
		return array_keys($this->data);
	}

	public function table_name()
	{
		return $this->table_name;
	}

	public function get($primary_key)
	{
		if ( ! $primary_key)
			return FALSE;

		return $this->db
			->where($this->primary_key, $primary_key)
			->get($this->table_name)
			->current();
	}

	public function get_ids($conditions = array(), $order_type = 'asc', $as_string = FALSE, $delimiter = ',')
	{
		$allowed_types = array('where', 'in');
		$types = array_keys($conditions);

		$compiled = array();
		foreach ($types as $type)
		{
			if (in_array($type, $allowed_types))
			{
				$compiled[$type] = $conditions[$type];
				continue;
			}
			$compiled['where'][$type] = $conditions[$type];
		}

		$query = $this->db->select($this->primary_key);

		foreach ($compiled as $key => $val)
		{
			$query->$key($val);
		}

		$selected_ids = $query->orderby($this->primary_key, $order_type)
			->get($this->table_name);

		$ids = array();

		if (count($selected_ids))
		{
			$selected_ids = iterator_to_array($selected_ids);

			$_primary_key = $this->primary_key;

			$ids = array_map(function ($row) use ($_primary_key)
				{
					return $row->$_primary_key;
				}, $selected_ids);
		}

		return ($as_string) ? implode($delimiter, $ids) : $ids;
	}

	public function get_by_tag($tag)
	{
		return $this->db
			->like('tag', $tag)
			->get($this->table_name);
	}

	public function prefix_where($condition)
	{
		$compiled = array();

		foreach ($condition as $key => $value)
		{
			if (strpos($key, '.') !== FALSE)
			{
				$compiled[$this->table_name.'.'.$key] = $value;
			}
			else
			{
				$compiled[$key] = $value;
			}
		}

		return $compiled ?: NULL;
	}

	public function delete_in(array $ids_to_delete)
	{
		return $this->db
			->in($this->primary_key, $ids_to_delete)
			->delete($this->table_name);
	}

} // End Auto_Modeler