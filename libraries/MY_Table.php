<?php

class Table extends Table_Core {

	public function render($echo = FALSE, $pre_html = '', $post_html = '')
	{
		// update existing data
			$this->update(FALSE);

		// attributes
			$attributes = '';
			foreach($this->table_attributes as $key => $value)
			{
				$attributes .= $key .'="' . $value .'" ';
			}

		// open the table
			$html = $pre_html;
			$html .= '<table ' . $attributes . '>' . $this->newline;

		// caption
			$html .= $this->_generate_caption();

		// colgroup
			if($this->column_attributes != NULL)
			{
				$html .= $this->_generate_colgroup();
			}

		// heading
			if($this->column_titles != NULL || $this->get_heading_cell_callback != NULL)
			{
				$html .= $this->_generate_heading();
			}

		// footer
			if($this->foot_html != NULL)
			{
				$html .= $this->_generate_footer();
			}

		// has structure
			$has_structure = $this->foot_html != NULL || $this->head_html != NULL;

		// body
			$html .= ($has_structure ? '<tbody>' : '') . $this->newline;
			$html .= $this->body_html;
			$html .= ($has_structure ? '</tbody>' : '') . $this->newline;

		// close table
			$html .= '</table>' . $this->newline;

			$html .= $post_html;

		// echo
			if($echo)
			{
				echo $html;
			}

		// return
			return $html;
	}

}