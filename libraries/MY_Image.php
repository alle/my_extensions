<?php defined('SYSPATH') OR die('No direct access allowed.');

class Image extends Image_Core {

	public function get_driver()
	{
		return $this->driver;
	}

	public function supported($method)
	{
		if (Kohana::config('image.driver') === 'GD')
		{
			switch ($method)
			{
				case 'sharpen':
					if ( ! function_exists('imageconvolution'))
					{
						Kohana::log('debug', sprintf(Kohana::lang('image.unsupported_method'), $method));
						return FALSE;
					}
			}
		}
		return TRUE;
	}

}