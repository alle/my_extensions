<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 * Helper for Ajax upload with the javascript library Plupload.
 *
 * @name Plupload
 *
 * @author Ambroise Dhenain.
 *
 * @since 1.0
 * @version 1.0
 */
class Plupload extends Upload {

    /**
     * Target where store the file. [assets/uploads/]
     * @var string
     */
    protected static $target_dir;

    /**
     * File name. If false, get the name of the file in the current request. [false]
     * @var string
     */
    protected static $file_name;

    /**
     * If true, clean the target directory, clean the old temp files. [true]
     * @var boolean
     */
    protected static $cleanup_target_dir = false;

    /**
     * The max file age in second. [18000]
     * @var int
     */
    protected static $max_file_age = 18000;

    /**
     * The time limit of the script. [360]
     * @var int
     */
    protected static $time_limit = 360;

    /**
     * If chunk is enable.
     * @var int
     */
    protected static $chunk;

    /**
     *
     * @var int
     */
    protected static $chunks;

    /**
     * Initialize the values. Call the protected _plupload for upload the image(s).
     *
     * @param string $target_dir - Target where store the file. [assets/uploads/]
     * @param string $file_name - File name. If false, get the name of the file in the current request. [false]
     * @param boolean $cleanup_target_dir - If true, clean the target directory, clean the old temp files. [true]
     * @param int $max_file_age_hours - The max file age in hours. [5]
     * @param int $time_limit_minute - The time limit of the script. [5]
     *
     * return array.
     */
    public static function upload($target_dir = 'assets/uploads/', $file_name = false, $cleanup_target_dir = true, $max_file_age_hours = 5, $time_limit_minute = 5) {
        // Initialize.
        self::$target_dir = $target_dir;
        self::$cleanup_target_dir = $cleanup_target_dir;
        self::$max_file_age = $max_file_age_hours * 3600;
        self::$time_limit = $time_limit_minute * 60;

        // If no filename provided, use the filename in the request sanitized.
        if ($file_name === false) {
            self::$file_name = isset($_REQUEST["name"]) ? self::sanitize_file_name($_REQUEST["name"]) : '';
        } else {
            // Else, use the filename provided, and sanitize, get the ext from the REQUEST file name.
            self::$file_name = self::sanitize_file_name($file_name);
        }

        // Get parameters.
        self::$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        self::$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        return self::_upload();
    }

    /**
     * Sanitize the file name and returns.
     *
     * @param string $file_name - The file name to sanitize.
     *
     * @return string
     */
    public static function sanitize_file_name($file_name) {
        return preg_replace('/[^\w\._]+/', '_', $file_name);
    }

    /**
     * *******************************************************************************************
     * ******************************** Protected ************************************************
     * *******************************************************************************************
     */

    /**
     * Upload the image(s) with the parameters initialized before. Can merge a file with several chunk parts.
     *
     * @return array
     */
    protected static function _upload() {
        // 5 minutes execution time
        @set_time_limit(self::$time_limit);

        // Make sure the fileName is unique but only if chunking is disabled
        if (self::$chunks < 2 && file_exists(self::$target_dir . DIRECTORY_SEPARATOR . self::$file_name)) {
            $ext = strrpos(self::$file_name, '.');
            $file_name_a = substr(self::$file_name, 0, $ext);
            $file_name_b = substr(self::$file_name, $ext);

            $count = 1;
            while (file_exists(self::$target_dir . DIRECTORY_SEPARATOR . $file_name_a . '_' . $count . $file_name_b))
                $count++;

            self::$file_name = $file_name_a . '_' . $count . $file_name_b;
        }

        $file_path = self::$target_dir . DIRECTORY_SEPARATOR . self::$file_name;

        // Create target dir
        if (!file_exists(self::$target_dir))
            @mkdir(self::$target_dir);

        // Remove old temp files
        if (self::$cleanup_target_dir) {
            if (is_dir(self::$target_dir) && ($dir = opendir(self::$target_dir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmp_file_path = self::$target_dir . DIRECTORY_SEPARATOR . $file;

                    // Remove temp file if it is older than the max age and is not the current file
                    if (preg_match('/\.part$/', $file) && (filemtime($tmp_file_path) < time() - self::$max_file_age) && ($tmp_file_path != "{$file_path}.part")) {
                        @unlink($tmp_file_path);
                    }
                }
                closedir($dir);
            } else {
                return self::_response(Kohana::lang('upload.temp_files_not_allowed'), array('code' => 100));
            }
        }

		$content_type = NULL;

        // Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $content_type = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $content_type = $_SERVER["CONTENT_TYPE"];

        // Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
        if (strpos($content_type, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
                // Open temp file
                $out = @fopen("{$file_path}.part", self::$chunk == 0 ? "wb" : "ab");
                if ($out) {
                    // Read binary input stream and append it to temp file
                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else {
                        return self::_response(Kohana::lang('plupload.error_reading_source_file'), array('code' => 101));
                    }
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else {
                    return self::_response(Kohana::lang('plupload.error_writing_temp_file'), array('code' => 102));
                }
            } else {
                return self::_response(Kohana::lang('plupload.error_moving_upload'), array('code' => 103));
            }
        } else {
            // Open temp file
            $out = @fopen("{$file_path}.part", self::$chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else {
                    return self::_response(Kohana::lang('plupload.error_reading_source_file'), array('code' => 101));
                }

                @fclose($in);
                @fclose($out);
            } else {
                return self::_response(Kohana::lang('plupload.error_writing_temp_file'), array('code' => 102));
            }
        }

        // Check if file has been uploaded
        if (!self::$chunks || self::$chunk == self::$chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$file_path}.part", $file_path);
        }

        return self::_response(Kohana::lang('plupload.upload_complete'), array(
            'target_dir' => self::$target_dir,
            'file_name' => self::$file_name,
        ), true);
    }

    /**
     * Return an array preconfigurated.
     *
     * @param array $message
     * @param string $data
     * @param string $status
     *
     * @return array
     */
    protected static function _response($message, $data = array(), $status = false) {
        return array(
            'message' => $message,
            'data' => array_merge($data, array('jsonrpc' => '2.0')),
            'status' => $status,
        );
    }

}