<?php defined('SYSPATH') OR die('No direct access allowed.');

$lang = array(

	'unsupported' => 'Il tuo browser non supporta Flash, Silverlight, Gears, BrowserPlus o HTML5.',

	'temp_files_not_allowed'    => 'Impossibile aprire il folder temporaneo.',
	'error_reading_source_file' => 'Impossibele aprire il flusso di lettura.',
	'error_writing_temp_file'   => 'Impossibile aprire il flusso di scrittura.',
	'error_moving_upload'       => 'Impossibile spostare il file caricato.',

	'upload_complete'           => 'Il caricamento è stato completato correttamente.'

);