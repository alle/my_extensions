<?php defined('SYSPATH') or die('No direct access allowed.');

$lang = array(
	'in' => 'fra %s giorni',
	'from' => 'da %s giorni',

	-2 => 'dopodomani',
	-1 => 'domani',
	 0 => 'oggi',
	 1 => 'ieri',
	 2 => 'l\'altro ieri',
);