<?php defined('SYSPATH') OR die('No direct access allowed.');

$lang = array(

	'unsupported' => 'You browser doesn\'t have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.',

	'temp_files_not_allowed'    => 'Unable to open the temporary directory.',
	'error_reading_source_file' => 'Unable to open the input stream.',
	'error_writing_temp_file'   => 'Unable to open the output stream.',
	'error_moving_upload'       => 'Could not move uploaded file.',

	'upload_complete'           => 'The file has been successfully uploaded.'

);