<?php defined('SYSPATH') OR die('No direct access allowed.');

$lang = array(

	'unsupported' => 'You browser doesn\'t have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.',

	'temp_files_not_allowed'    => 'Impossible d\'ouvrir le répertoire temporaire.',
	'error_reading_source_file' => 'Impossible d\'ouvrir le flux d\'entrée.',
	'error_writing_temp_file'   => 'Impossible d\'ouvrir le flux de sortie.',
	'error_moving_upload'       => 'Impossible de déplacer le fichier téléchargé.',

	'upload_complete'           => 'Le fichier a été correctement téléchargé.'

);